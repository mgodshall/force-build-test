# Copyright (C) 2016 Unknow User <unknow@user.org>
# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "Test recipe for force-rebuild-tag.bbclass"
LICENSE = "GPL"
LIC_FILES_CHKSUM = "file://README.md;md5=05e3c53a03a1edf6ffc001b7a71cdbf0"
PR = "r0"

inherit force-rebuild-tag allarch

S = "${WORKDIR}/git"

SRC_URI = "git://git@bitbucket.org/mgodshall/force-build-test.git;protocol=ssh;tag=v1.0.0"

do_install() {
    install -d ${D}/usr/share/doc/force-rebuild-test
    install ${S}/README.md ${D}/usr/share/doc/force-rebuild-test
}

FILES_${PN} = " \
    /usr \
    "
